angular.module('ugryzSzczescie', [])
  .controller('MenuController', function() {

    var menuController = this;
 
	menuController.menu = [];
    menuController.calories = 0;
    menuController.sex = "woman";
    menuController.activity = "1.0";
    menuController.diet = "slim";
    menuController.preferences = "none";
	 
    menuController.createMenu = function() {
        menuController.countCalories();
	  
	  var menu = emptyMenu;
	  
	  // dla każdego dnia tygodnia
	  for (var i=0; i<menu.length; i++) {
		  // dla każdego posiłku
		  for(var j=0; j<menu[i].meals.length; j++) {
			var caloriesSum = 0; // suma kalorii dla jednego posiłku
            menu[i].meals[j].caloriesLimit = Math.round(menuController.calories * menu[i].meals[j].caloriesLimitPercent, 0);
			
            var caloriesLimit = menu[i].meals[j].caloriesLimit;               
			var name = menu[i].meals[j].name; 
              
			
				var	product = pickProductForMeal(name, caloriesLimit, menuController.preferences);
                if(product != undefined){
                    menu[i].meals[j].products.push(product);  // dodajemy produkt do posiłku
                    caloriesSum = caloriesSum + product.calories; // doliczamy kalorie z produktu do posiłku
                }				
			
			menu[i].meals[j].calories = caloriesSum; // przepisanie wyniku do posilku
		  }
	  }
          
	  menuController.menu = menu;	 
      menuController.message = "Stworzyliśmy dla Ciebie super jadłospis!";
    };
    
    menuController.countCalories = function(){
        var sexFactor =  0;
        if(menuController.sex == 'woman'){
            sexFactor = -161;
        }else{
            sexFactor = 5;
        }
        var bmr = 9.99 * menuController.weight + 6.25 * menuController.height - 4.92 * menuController.age + sexFactor;
        var activityFactor = menuController.activity;
        menuController.calories = Math.round(bmr * activityFactor,0);
    };    
	
 });
 
function pickProductForMeal(mealName, caloriesLimit, preferences) {	
    var receiptOneMeal=[];
    var minCalories = 0.9 * caloriesLimit;
    var maxCalories = 1.1 * caloriesLimit;
    
    for(var i = 0; i<allMeals.length; i++) {
        var tempPreferences = (preferences == "nomeat" && allMeals[i].noMeat) || (preferences == "vege" && allMeals[i].vege) || preferences == "none";
        if (mealName == "Śniadanie" && allMeals[i].breakfast && tempPreferences && allMeals[i].calories > minCalories && allMeals[i].calories < maxCalories) {
            receiptOneMeal.push(allMeals[i]);
        }
        if (mealName == "Drugie śniadanie" && allMeals[i].secondBreakfast && tempPreferences && allMeals[i].calories > minCalories && allMeals[i].calories < maxCalories) {
            receiptOneMeal.push(allMeals[i]);
        }
        if (mealName == "Obiad" && allMeals[i].dinner && tempPreferences && allMeals[i].calories > minCalories && allMeals[i].calories < maxCalories) {
            receiptOneMeal.push(allMeals[i]);
        }
        if (mealName == "Deser" && allMeals[i].dessert && tempPreferences && allMeals[i].calories > minCalories && allMeals[i].calories < maxCalories) {
            receiptOneMeal.push(allMeals[i]);
        }
        if (mealName == "Kolacja" && allMeals[i].supper && tempPreferences && allMeals[i].calories > minCalories && allMeals[i].calories < maxCalories) {
            receiptOneMeal.push(allMeals[i]);
        }
    }
    
    
	var randomIndex = Math.round(Math.random()*(receiptOneMeal.length-1));
	return receiptOneMeal[randomIndex];
}
 
var allMeals = [
	{   id:1,
        name:"bułka z plasterkiem szynki", 
        description: "",
        calories: 300,
        breakfast: true,
        secondBreakfast: false,
        dinner: false,
        dessert: false,
        supper: false,
        noMeat: false,
        vege: false,
        protein: 10,
        carbohydrates: 1,
        fats: 4,
    },
    {   id:2,
        name:"owsianka na mleku z żurawiną i wiśniami", 
        description: "",
        calories: 280,
        breakfast: true,
        secondBreakfast: true,
        dinner: false,
        dessert: true,
        supper: true,
        noMeat: true,
        vege: true,
        protein: 20,
        carbohydrates: 10,
        fats: 1,
    },
        {   id:3,
        name:"naleśnik z dżemem jagodowym", 
        description: "",
        calories: 280,
        breakfast: true,
        secondBreakfast: true,
        dinner: true,
        dessert: true,
        supper: true,
        noMeat: true,
        vege: true,
        protein: 10,
        carbohydrates: 20,
        fats: 5,
    },
        {   id:4,
        name:"chłodnik z ogórkiem, rzodkiewką, koperkiem", 
        description: "",
        calories: 275,
        breakfast: true,
        secondBreakfast: true,
        dinner: true,
        dessert: true,
        supper: true,
        noMeat: true,
        vege: true,
        protein: 20,
        carbohydrates: 10,
        fats: 0,
    },
      {   id:5,
        name:"Kasza bulgur z warzywami", 
        description: "",
        calories: 275,
        breakfast: true,
        secondBreakfast: true,
        dinner: true,
        dessert: true,
        supper: true,
        noMeat: true,
        vege: true,
        protein: 20,
        carbohydrates: 10,
        fats: 5,
    },
     {   id:6,
        name:"Makaron z kurczakiem, szpinakiem i serem mozzarella", 
        description: "",
        calories: 575,
        breakfast: false,
        secondBreakfast: false,
        dinner: true,
        dessert: false,
        supper: false,
        noMeat: false,
        vege: false,
        protein: 50,
        carbohydrates: 10,
        fats: 35,
    },
    {   id:7,
        name:"Makaron ze szpinakiem i serem mozzarella", 
        description: "",
        calories: 575,
        breakfast: false,
        secondBreakfast: false,
        dinner: true,
        dessert: false,
        supper: false,
        noMeat: true,
        vege: true,
        protein: 40,
        carbohydrates: 10,
        fats: 25,
    },
    {   id:8,
        name:"Koktajl z mango", 
        description: "",
        calories: 185,
        breakfast: true,
        secondBreakfast: true,
        dinner: false,
        dessert: false,
        supper: false,
        noMeat: true,
        vege: true,
        protein: 5,
        carbohydrates: 10,
        fats: 0,
    },
     {   id:9,
        name:"Zapiekanka z łososiem", 
        description: "",
        calories: 565,
        breakfast: false,
        secondBreakfast: false,
        dinner: true,
        dessert: false,
        supper: false,
        noMeat: true,
        vege: false,
        protein: 55,
        carbohydrates: 10,
        fats: 20,
    },
];
  
 var emptyMenu = [
		{name:"Poniedziałek", meals: [
			{name:"Śniadanie", calories: 0, caloriesLimit: 0, caloriesLimitPercent: 0.2, products:[]},
			{name:"Drugie śniadanie", calories: 0, caloriesLimit: 0, caloriesLimitPercent: 0.133, products:[]},
			{name:"Obiad", calories: 0, caloriesLimit: 0, caloriesLimitPercent: 0.4, products:[]},
			{name:"Deser", calories: 0, caloriesLimit: 0, caloriesLimitPercent: 0.133, products:[]},
			{name:"Kolacja", calories: 0, caloriesLimit: 0, caloriesLimitPercent: 0.133, products:[]}
		]}/*,
		{name:"Wtorek", meals: [
			{name:"Śniadanie", calories: 0, products:[]},
			{name:"Drugie śniadanie", calories: 0, products:[]},
			{name:"Obiad", calories: 0, products:[]},
			{name:"Deser", calories: 0, products:[]},
			{name:"Kolacja", calories: 0, products:[]}
		]},
		{name:"Środa", meals: [
			{name:"Śniadanie", calories: 0, products:[]},
			{name:"Drugie śniadanie", calories: 0, products:[]},
			{name:"Obiad", calories: 0, products:[]},
			{name:"Deser", calories: 0, products:[]},
			{name:"Kolacja", calories: 0, products:[]}
		]},
		{name:"Czwartek", meals: [
			{name:"Śniadanie", calories: 0, products:[]},
			{name:"Drugie śniadanie", calories: 0, products:[]},
			{name:"Obiad", calories: 0, products:[]},
			{name:"Deser", calories: 0, products:[]},
			{name:"Kolacja", calories: 0, products:[]}
		]},
		{name:"Piątek", meals: [
			{name:"Śniadanie", calories: 0, products:[]},
			{name:"Drugie śniadanie", calories: 0, products:[]},
			{name:"Obiad", calories: 0, products:[]},
			{name:"Deser", calories: 0, products:[]},
			{name:"Kolacja", calories: 0, products:[]}
		]},
		{name:"Sobota", meals: [
			{name:"Śniadanie", calories: 0, products:[]},
			{name:"Drugie śniadanie", calories: 0, products:[]},
			{name:"Obiad", calories: 0, products:[]},
			{name:"Deser", calories: 0, products:[]},
			{name:"Kolacja", calories: 0, products:[]}
		]},
		{name:"Niedziela", meals: [
			{name:"Śniadanie", calories: 0, products:[]},
			{name:"Drugie śniadanie", calories: 0, products:[]},
			{name:"Obiad", calories: 0, products:[]},
			{name:"Deser", calories: 0, products:[]},
			{name:"Kolacja", calories: 0, products:[]}
		]}*/		
	  ];
